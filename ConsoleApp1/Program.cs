﻿using System.IO.Compression;

namespace ConsoleApp1
{
    internal class Program
    {
       
        static void Main(string[] args)
        {
         

            string name = "Muad'Dub", street = "Sietch Tabor", city = "Arrakeen", state = "Arrakis", zip = "42132" ;
            int apartmentNumber = 1;

            Console.WriteLine("To: " + name);
            Console.WriteLine(street + " Appt: " + apartmentNumber);
            Console.WriteLine(city + ", " + state + " " + zip);
        }
    }
}